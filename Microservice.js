const express = require('express');
const bodyParser = require('body-parser');
const logger = require('winston');
const mongoose = require('mongoose');
const helmet = require('helmet');
const cors = require('cors');
const Rp = require('hp-rp-factory');

const rp = new Rp([
  'ERROR_404',
  'ERROR_500'
]);

class Microservice {
  constructor(options) {
    this.app = express();
    this.routers = express.Router();
    this.options = options;
  }

  init() {
    this.app.use(helmet());
    this.app.use(cors());
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));

    this.options.routes.forEach((route) => {
      const middlewares = route.middlewares || [];
      this.routers[route.method](route.path, middlewares, route.func);
    });

    this.app.use(this.routers);

    // catch error
    this.app.use((req, res) => rp.error(res, 'ERROR_404'));
    this.app.use((err, req, res) => rp.error(res, 'ERROR_500'));

    if (this.options.mongoUrl) {
      mongoose.Promise = Promise;
      mongoose
        .connect(this.options.mongoUrl)
        .then(() => logger.info('Connected to MongoDB'));
    }

    this.app.listen(this.options.port, () => logger.info(`Connected to port ${this.options.port}`));
  }
}

module.exports = Microservice;